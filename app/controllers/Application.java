package controllers;

import models.Post;
import models.Role;
import models.User;
import org.apache.commons.lang3.StringUtils;
import play.api.data.validation.ValidationError;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Lang;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import scala.collection.Seq;
import scala.collection.mutable.*;
import scala.collection.mutable.StringBuilder;
import views.html.*;


import java.util.*;

import static play.data.Form.form;

@With(LanguageAction.class)
public class Application extends Controller {

    static Form<ChangePassword> changePasswordForm = Form.form(ChangePassword.class);

    public static Result index() {
        List<Post> posts = Post.find.all();
        return ok(index.render(posts));
    }

    public static Result createForm() {
        Form<User> userForm = form(User.class);
        return ok(template.render(userForm));
    }

    public static Result submit() {
        Form<User> userForm = form(User.class).bindFromRequest();
        if (userForm.hasErrors()) {
            return badRequest(template.render(userForm));
        } else {
            User user = userForm.get();
            return ok("Got user " + user);
        }
    }

    public static Result registration() {
        java.util.Map<String, String> map = new java.util.HashMap<>();
        map.put("username", "");
        map.put("password", "");
        map.put("passwordConfirmation", "");
        DynamicForm userData = Form.form().fill(map);

        return ok(signUp.render(userData));
    }

    public static Result changePassword() {
        Form<ChangePassword> pass = form(ChangePassword.class).bindFromRequest();
        if (pass.hasErrors()) {
            return badRequest(changePassword.render(pass));
        } else {
            User user = User.find.byId(Secured.user().id);
            if (user != null) {
                user.password = pass.get().newPassword;
                user.update();
            }
            return redirect("/");
        }
    }

    public static Result renderChangePassword() {
        return ok(changePassword.render(changePasswordForm));
    }

    public static Result register() {
        DynamicForm requestData = Form.form().bindFromRequest();
        String username = requestData.get("username");
        String password = requestData.get("password");
        String passwordConfirmation = requestData.get("passwordConfirmation");

        boolean usernameDuplicated = User.find.where().eq("username", username).findUnique() != null;
        if(usernameDuplicated){
          requestData.reject("username", "Username already used");
        }

        boolean passowrdMatches = password != null ?  password.equals(passwordConfirmation) : false;
        if(!passowrdMatches){
          requestData.reject("password", "Passwords don't match");
          requestData.reject("passwordConfirmation", "Passwords don't match");
        }

        boolean passwordLengthOk = password != null ? (password.length() > 5 && (password.length() < 16)) : false;
        boolean passwordConfirmationLengthOk = passwordConfirmation != null ? (passwordConfirmation.length() > 5 && (passwordConfirmation.length() < 16)) : false;
        if(!passwordLengthOk){
          requestData.reject("password", "Password's length is incorrect");
        }
        if(!passwordConfirmationLengthOk){
          requestData.reject("passwordConfirmation", "Password's length is incorrect");
        }

        if (requestData.hasErrors()) {
            return badRequest(signUp.render(requestData));
        } else {
            User user = new User();
            user.password = password;
            user.username = username;
            user.role = Role.find.where().eq("type", Role.Type.EDITOR).findUnique();
            user.save();
            return redirect("/");
        }
    }

    public static Result login() {
        return ok(
                login.render(form(Login.class))
        );
    }

    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(login.render(loginForm));
        } else {
            String previousCallUri = session("back_url");

            session().clear();
            session("username", loginForm.get().username);

            if (previousCallUri != null) {
                return redirect(previousCallUri);
            } else {
                return redirect(routes.Application.index());
            }
        }
    }

    public static Result logout() {
        session().clear();
        flash("success", "You've been logged out");
        return redirect(
                routes.Application.login()
        );
    }

    public static class Login {
        public String username;
        public String password;

        public String validate() {
            if (User.authenticate(username, password) == null) {
                return "Invalid username or password";
            }
            return null;
        }
    }

    public static class ChangePassword {
        public String originalPassword;
        public String newPassword;
        public String newPasswordConfirmation;

        public String validate() {
            if (Secured.user() != null && !Secured.user().password.equals(originalPassword)
                    || newPassword != null && !newPassword.equals(newPasswordConfirmation)) {
                return "Password doesn't match";
            }
            return null;
        }

    }

}
