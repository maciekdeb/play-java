package controllers;

import models.Comment;
import models.Post;
import models.User;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import views.html.editPost;
import views.html.postDetails;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
@With(LanguageAction.class)
public class PostsEditor extends Controller {

    public static Result showEmpty() {
        Form<Post> postForm = form(Post.class);
        User user = Secured.user();
        if (user != null) {
            postForm = postForm.fill(new Post(user.username));
        } else {
            postForm = postForm.fill(new Post(""));
        }
        return ok(editPost.render(postForm));
    }

    public static Result showById(Long id) {
        Post post = Post.find.byId(id);
        if (post != null) {
            Form<Post> postForm = form(Post.class);
            postForm = postForm.fill(post);
            return ok(editPost.render(postForm));
        } else {
            return redirect(routes.PostsEditor.showEmpty());
        }
    }

    public static Result submit() {
        Form<Post> postForm = form(Post.class).bindFromRequest();
        if (postForm.hasErrors()) {
            return badRequest(editPost.render(postForm));
        } else {
            Post post = postForm.get();
            if (post.id == null) {
                post.save();
            } else {
                post.update();
            }
            return redirect(routes.PostsViewer.showAndUpdatePost(post.id));
        }
    }

}
