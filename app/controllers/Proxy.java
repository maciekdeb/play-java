package controllers;


import play.libs.F.Function;
import play.libs.F.Promise;
import play.libs.ws.*;
import play.mvc.Controller;
import play.mvc.Result;


public class Proxy extends Controller {

    public static Promise<Result> index(String url) {
        Promise<WSResponse> responsePromise = WS.url(url).get();

        return responsePromise.map(new Function<WSResponse, Result>() {
            @Override
            public Result apply(WSResponse response) throws Throwable {
                return ok(response.getBody()).as("text/html");
            }
        });
    }

}
