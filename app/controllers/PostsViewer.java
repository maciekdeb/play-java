package controllers;

import models.Comment;
import models.Post;
import models.User;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import views.html.editPost;
import views.html.postDetails;

import static play.data.Form.form;

@With(LanguageAction.class)
public class PostsViewer extends Controller {

    public static Result addComment(Long id) {
        Form<Comment> commentForm = form(Comment.class).bindFromRequest();
        if (commentForm.hasErrors()) {
            Logger.error("Comment has errors...");
        } else {
            Post post = Post.find.byId(id);
            if (post != null) {
                Comment comment = commentForm.get();
                comment.mark = 0L;
                post.comments.add(comment);
                post.update();
            }
        }
        return redirect(routes.PostsViewer.showAndUpdatePost(id));
    }

    public static Result showAndUpdatePost(Long id) {
        String commentIdStr = form().bindFromRequest().get("commentId");
        String thumb = form().bindFromRequest().get("thumb");
        if (commentIdStr != null && thumb != null) {
            try {
                Long commentId = Long.parseLong(commentIdStr);
                Comment comment = Comment.find.byId(commentId);
                if (comment != null) {
                    if ("up".equals(thumb)) {
                        comment.mark++;
                    } else if ("down".equals(thumb)) {
                        comment.mark--;
                    }
                    comment.update();
                }
            } catch (Exception e) {
                Logger.error("Marking comments params weren't available...");
            }
        }
        Post post = Post.find.byId(id);
        Form<Comment> commentForm = Form.form(Comment.class);
        commentForm = commentForm.fill(new Comment());
        return ok(postDetails.render(post, commentForm));
    }

}
