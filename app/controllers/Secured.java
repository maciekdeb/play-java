package controllers;

import models.User;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import static play.mvc.Controller.session;

/**
 * Created by maciej.debowski on 2015-05-02.
 */
public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Http.Context ctx) {
        return ctx.session().get("username");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        session("back_url", ctx.request().uri());
        return redirect(routes.Application.login());
    }

    public static User user() {
        return User.find.where().eq("username", session().get("username")).findUnique();
    }
}