package controllers;

import org.apache.commons.lang3.StringUtils;
import play.Logger;

import play.i18n.Lang;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

/**
 * Created by maciek on 14.06.15.
 */
public class LanguageAction extends Action.Simple {

    public F.Promise<Result> call(Http.Context ctx) throws Throwable {
        String language = ctx.request().getQueryString("lang");

        if (StringUtils.isNotBlank(language)) {
            Logger.info("Calling action with a change language request (for " + language + ").");
            ctx.changeLang(Lang.forCode(language));
        }

        return delegate.call(ctx);
    }
}
