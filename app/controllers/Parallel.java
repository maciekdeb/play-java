package controllers;


import play.libs.F.Function;
import play.libs.F.Promise;
import play.libs.Json;
import play.libs.ws.WS;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Parallel extends Controller {

    public static Promise<Result> index() {

        final long start = System.currentTimeMillis();

        Function<WSResponse, Long> timing = new Function<WSResponse, Long>() {
            @Override
            public Long apply(WSResponse wsResponse) throws Throwable {
                return System.currentTimeMillis() - start;
            }
        };

        final Promise<Long> yahoo = WS.url("http://www.yahoo.com").get().map(timing);
        final Promise<Long> google = WS.url("http://www.google.com").get().map(timing);
        final Promise<Long> bing = WS.url("http://www.bing.com").get().map(timing);

        return Promise.sequence(yahoo, google, bing).map(new Function<List<Long>, Result>() {
            @Override
            public Result apply(List<Long> wsResponses) throws Throwable {
                Map<String, Long> data = new HashMap<String, Long>();
                data.put("yahoo", yahoo.get(0));
                data.put("google", google.get(0));
                data.put("bing", bing.get(0));

                return ok(Json.toJson(data));
            }
        });


    }

}
