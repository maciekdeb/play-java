package models;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Post extends Model {

  @Id
  public Long id;

  @Constraints.Required
  @javax.persistence.Column(length=500)
  public String title;

  @Constraints.Required
  public String author;

  @Constraints.Required
  @javax.persistence.Column(length=5000)
  public String content;

  @OneToMany(cascade = CascadeType.ALL)
  public List<Comment> comments = new ArrayList<>();

  @Formats.DateTime(pattern = "dd.MM.yyyy HH:mm")
  public Date publishedOn = new Date();

  public Post(String author) {
    this.author = author;
  }

  public Post(String title, String content) {
    this.title = title;
    this.content = content;
  }

  public static Finder<Long, Post> find = new Finder<Long, Post>(
    Long.class, Post.class
  );

}
