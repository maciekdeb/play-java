package models;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Constraint;
import java.util.Date;

@Entity 
public class Comment extends Model {

  @Id
  public Long id;

  @Constraints.Required
  public String author;

  @Constraints.Required
  public String content;

  @ManyToOne
  public Post post;

  public Long mark;

  @Formats.DateTime(pattern = "dd/MM/yyyy")
  public Date publishedOn = new Date();

  public Comment() {

  }

  public Comment(Post post, String author, String content) {
    this.post = post;
    this.author = author;
    this.content = content;
  }

  public static Finder<Long, Comment> find = new Finder<Long, Comment>(
    Long.class, Comment.class
  );

}
