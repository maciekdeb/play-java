package models;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.Constraint;
import java.util.Date;

@Entity 
public class User extends Model {

  @Id
  @Constraints.Min(10)
  public Long id;
  
  @Constraints.Required
  @Formats.NonEmpty
  @Column(unique = true)
  public String username;

  @Constraints.Required
  public String password;

  @ManyToOne(cascade = CascadeType.ALL)
  public Role role;

  public static Finder<Long, User> find = new Finder<Long, User>(
    Long.class, User.class
  );

  public static User authenticate(String username, String password) {
    User user = find.where().eq("username", username).findUnique();
    if (user != null) {
      if (password != null && password.equals(user.password)) {
        return user;
      }
    }
    return null;
  }
}
