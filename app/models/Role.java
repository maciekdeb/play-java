package models;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity 
public class Role extends Model {

  @Id
  @Constraints.Min(10)
  public Long id;
  
  @Constraints.Required
  @Formats.NonEmpty
  @Column(unique = true)
  public Type type;

  public enum Type {
    EDITOR, ADMIN, USER;
  }

  @OneToMany(cascade = CascadeType.ALL)
  public List<User> users = new ArrayList<>();

  public void setType(Type type) {
    this.type = type;
  }

  public static Finder<Long, Role> find = new Finder<Long, Role>(
    Long.class, Role.class
  );

}
