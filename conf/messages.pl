label.title = Środowiska aplikacyjne Java EE

NotEmpty.user.email=Podaj adres email
Email.user.email=Podany email jest niepoprawny

userForm.password.empty=Haslo jest puste
userForm.username.empty=Nazwa uzytkownika jest pusta
userForm.email.empty=Adres e-mail jest pusty
userForm.email.notvalid=Adres e-mail jest niepoprawny
userForm.email.alreadyExists=Konto z takim adresem e-mail już istnieje
userForm.username.alreadyExists=Konto z taką nazwą użytkownika już istnieje
userForm.password.wrongNumber=Hasło powinno zawierać od 6 do 15 znaków
userForm.password.mismatch=Hasła nie zgadzają się
userForm.passwordConfirmation.mismatch=Hasła nie zgadzają się

form.change.info.legend=Zmiana danych
form.change.info.email=Adres email
form.change.info.username=Nazwa u�ytkownika
form.change.info.submit=Zapisz

form.change.password.legend=Zmiana has�a
form.change.password.originalPassword=Poprzednie has�o
form.change.password.newPassword=Nowe has�o
form.change.password.newPasswordConfirmation=Potwierdzenie nowego hasła
form.change.password.submit=Zapisz

form.entry.details.addComment.submit=Dodaj komentarz
form.entry.details.comment.inDay=w dniu
form.entry.details.comment.wrote=napisać
form.entry.details.comment.reply=odpowiedz

form.entry.list.legend=Znajdź wpis
form.entry.list.title=Tytuł zawiera
form.entry.list.content=Treść zawiera
form.entry.list.author=Autor
form.entry.list.hashTags=HashTagi
form.entry.list.submit=Znajdź
form.entry.list.head=Wpisy pasujące do podanych warunków

index.title=Nowy blog system
index.content=Oto nowy system do blogowania oparty o Play Framework, umożliwia dodawanie wpisów, ich edycję
index.link=Przejdź dalej &raquo;
index.entry.list.head=Ostatnie wpisy
login.head.login=Zaloguj
login.username=Nazwa użytkownika
login.password=Hasło
login.signin=Zaloguj
login.reset=Wyczyść
login.register=Zarejestruj

navbar.toogglenavigation=Toggle navigation
navbar.brandlogo=PlayBlog
navbar.home=Home
navbar.find=Znajdź
navbar.addpost= Dodaj wpis
navbar.accessmanagement=\                                 Zarządzanie dostępem
navbar.accessmanagement.users=Uęytkownicy
navbar.accessmanagement.roles=Role
navbar.login=Zaloguj
navbar.signup=Zarejestruj
navbar.signin=Zaloguj
navbar.changeinfo=Zmień dane
navbar.changepassword=Zmień hasło
navbar.logout=Wyloguj

role.details.legend=Szczegły roli
role.details.name=Nazwa
role.details.submit=Zapisz
role.list.name=Nazwa
role.list.identificator=Identyfikator
role.list.edit=Edycja
role.list.delete=Usuń

signup.legend=Rejestracja
signup.submit=Zarejestruj
signup.passwordConfirmation.info=Potwierdź hasło
signup.passwordConfirmation=Hasło (powtórzenie)
signup.password.info=Hasło powinno składać się z od 6 do 15 znaków
signup.password=Hasło
signup.email.info=Podaj nazwę użytkownika
signup.email=Nazwa użytkownika
signup.username.info=Nazwa użytkownika może zawierać tylko litery lub liczby bez spacji
signup.username=Nazwa użytkownika

user.edit.submit=Zapisz
user.edit.banned=Zbanowany
user.edit.active=Aktywny
user.edit.confirmed=Potwierdzony
user.edit.roles=Role
user.edit.joindate=Data dołączenia
user.edit.email=Email
user.edit.legend=Edycja danych użytkownika
user.edit.username=Nazwa użytkownika

user.list.edit=Edycja
user.list.delete=Usuń
form.entry.editable.title=Tytuł
form.entry.editable.content=Treść
form.entry.editable.hashTags=Tagi oddzielone przecinkami (możesz użyć spacji w jednym tagu, np. dobra rzecz stworzy \#dobra rzecz)

user.list.id=Id
user.list.username=Nazwa użytkownika
user.list.email=Adres email
user.list.confirmed=Potwierdzone
user.list.locked=Zablokowane

navbar.language=Język
form.entry.details.editpost=Edytuj wpis
info.succesfullchange=Dane zostały pomyślnie zmienione
registration.info=Potwierdź proszę swoją rejestrację poprzez kliknięcie linka w mailu
registration.login=Potwierdzenie zakończone. Teraz możesz się zalogować.
change.password.success=Hasło zostało zmienione
403.message=Nie masz dostępu.
login.invalid=Błędna nazwa użytkownika lub hasło\!

post.save=Zapisz
