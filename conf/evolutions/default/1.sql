# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table comment (
  id                        bigint auto_increment not null,
  author                    varchar(255),
  content                   varchar(255),
  post_id                   bigint,
  mark                      bigint,
  published_on              datetime,
  constraint pk_comment primary key (id))
;

create table post (
  id                        bigint auto_increment not null,
  title                     varchar(500),
  author                    varchar(255),
  content                   varchar(5000),
  published_on              datetime,
  constraint pk_post primary key (id))
;

create table role (
  id                        bigint auto_increment not null,
  type                      integer,
  constraint ck_role_type check (type in (0,1,2)),
  constraint uq_role_type unique (type),
  constraint pk_role primary key (id))
;

create table task (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  done                      tinyint(1) default 0,
  due_date                  datetime,
  constraint pk_task primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  username                  varchar(255),
  password                  varchar(255),
  role_id                   bigint,
  constraint uq_user_username unique (username),
  constraint pk_user primary key (id))
;

alter table comment add constraint fk_comment_post_1 foreign key (post_id) references post (id) on delete restrict on update restrict;
create index ix_comment_post_1 on comment (post_id);
alter table user add constraint fk_user_role_2 foreign key (role_id) references role (id) on delete restrict on update restrict;
create index ix_user_role_2 on user (role_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table comment;

drop table post;

drop table role;

drop table task;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

