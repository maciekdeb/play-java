/**
 * Created by maciek on 21.12.14.
 */
$(function () {

    var url = window.location;
    console.log(window.location);
// Will only work if string in href matches with location
    $('ul.nav a[href="'+ url +'"]').parent().addClass('active');

// Will also work for relative and absolute hrefs
    $('ul.nav a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');

});